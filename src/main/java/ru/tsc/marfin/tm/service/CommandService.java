package ru.tsc.marfin.tm.service;

import ru.tsc.marfin.tm.api.ICommandRepository;
import ru.tsc.marfin.tm.model.Command;

public class CommandService implements ru.tsc.marfin.tm.api.ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository){
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands(){
        return commandRepository.getTerminalCommands();
    }

}
