package ru.tsc.marfin.tm.api;

import ru.tsc.marfin.tm.model.Command;

public interface ICommandService {
    Command[] getTerminalCommands();
}
